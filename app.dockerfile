FROM php:7-fpm

WORKDIR /var/www

RUN apt-get update && apt-get install -y libmcrypt-dev zip unzip libpng-dev \
	wget gnupg mysql-client git zlib1g-dev --no-install-recommends \
    && docker-php-ext-install pdo_mysql zip

RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash
RUN apt-get install -y nodejs
RUN npm i -g yarn