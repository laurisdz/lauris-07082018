<?php

namespace App\Http\Controllers;

use App\Document;
use App\Http\Requests\DocumentUploadRequest;
use App\Http\Requests\SearchableRequest;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\File;

class DocumentController extends Controller
{
    /**
     * List all documents
     * @param SearchableRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(SearchableRequest $request)
    {
        // default response
        $res = [];

        $query = Document::query();

        if ($request->has("search")) {
            $query->where('title', 'like', '%' . $request->get("search") . '%');
            $query->orWhere('type', 'like', '%' . $request->get("search") . '%');
        }

        $res["documents"] = $query->orderBy("id", "desc")
            ->take(20)
            ->get();

        return response()->json($res);
    }

    /**
     * Handle document upload
     * Files are validated by DocumentUploadRequest class
     *
     * @param DocumentUploadRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function create(DocumentUploadRequest $request)
    {
        // default response
        $res = ["status" => "ok"];

        // upload file to documents storage
        $path = $request->file('file')->store('documents');
        $file = new File(storage_path('app/' . $path));


        // store information about file in database
        $doc = new Document;
        $doc->filename = $path;
        $doc->title = $request->file->getClientOriginalName();
        $doc->ip = $request->ip();
        $doc->type = $file->getMimeType();
        $doc->save();

        // return newly created doc to user
        $res["document"] = $doc->toArray();
        $res["path"] = $path;

        return response()->json($res);
    }

    /**
     * Delete document
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id, Request $request)
    {
        $res = ["status" => "ok"];

        $doc = Document::findOrFail($id);
        $doc->delete();

        return response()->json($res);
    }
}
