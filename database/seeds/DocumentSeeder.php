<?php

use Illuminate\Database\Seeder;

class DocumentSeeder extends Seeder
{
    const COUNT = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ["image/gif", "image/png", "image/jpeg"];

        for ($i = 0; $i < self::COUNT; ++$i) {
            $title = str_random(10);
            DB::table('documents')->insert([
                'title' => $title,
                'filename' => $title . '.jpg',
                'ip' => '::1',
                'type' => $types[rand(0, count($types) - 1)],
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
            ]);
        }
    }
}
