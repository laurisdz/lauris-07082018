# Docapp

## Demo

Live demo is available here: https://docapp.datazenit.com/

## Overview

App is built using Laravel, Typescript, React, Redux/Thunk and Material UI. App supports document uploading, listing, deleting and searching.

Documents are exposed through simple REST API under /api/document route. It accepts GET, POST, DELETE methods for common actions with documents. API requests are validated with Request classes which are located in app/Http/Requests directory.

React app can be found in resources/assets/js directory and it's composed of several React components for different parts of the app. The app uses actions, reducers and other patterns that are commonly used in modern React apps. REST API is accessed through DocumentApi repository class.

Document search is implemented as very simple fulltext matching against filename and file type columns. In real-world case a more elaborate method would be used for document searching, e.g., Elasticsearch.

Content Security Policy is handled by laravel-csp package. CSP configuration and policy can be found in these files: config/csp.php and app/Services/Csp/Policy.php.

Application is secured against SQLi, XSS and other generic attacks using common security practices.

## Setup

### Setup using Docker Compose (recommended)

1. Clone repository: `git clone git@bitbucket.org:laurisdz/lauris-07082018.git && cd docapp`
2. Copy `.env.example` to `.env`
3. Make sure that storage directory is writable
4. Build and start app services: `docker-compose up -d`
5. Install app dependencies and prepare the app:
	`docker-compose run --rm app composer install`
	`docker-compose run --rm app php artisan key:generate`
	`docker-compose run --rm app php artisan migrate --seed`
	`docker-compose run --rm app yarn`
6. Build React app for dev environment: `docker-compose run --rm app yarn run dev`
7. Open http://localhost:8080/

### Manual setup

1. Copy `.env.example` to `.env` and edit database credentials
2. Install Composer dependecies: `composer install`
3. Migrate database: `php artisan migrate --seed`
4. Install npm dependencies: `yarn`
5. Run React dev server: `yarn run dev`
6. Run Laravel app: `php artisan serve`
7. Open http://localhost:8000/

