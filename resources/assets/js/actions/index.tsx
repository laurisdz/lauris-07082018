import {Document} from "../types";
import DocumentApi from "../api/DocumentApi";
import {
  // UPLOAD_DOCUMENT,
  // RECEIVE_DOCUMENT,
  // DELETE_DOCUMENT,
  UPLOAD_DOCUMENT_SUCCESS,
  UPLOAD_DOCUMENT_FAILURE,
  RECEIVE_DOCUMENT_SUCCESS,
  RECEIVE_DOCUMENT_FAILURE,
  DELETE_DOCUMENT_SUCCESS,
  DELETE_DOCUMENT_FAILURE,
  DISMISS_MESSAGE, SEND_MESSAGE, SEARCH_DOCUMENT,
} from "../constants";

/**
 * Document uploading
 */

// upload successful action
export const uploadSuccessful =
  (document: Document) => (dispatch: any) => {
    dispatch(sendMessage("Document uploaded"));
    dispatch({type: UPLOAD_DOCUMENT_SUCCESS, document});
  };

// upload failed action
export const uploadFailed =
  (error: any) => (dispatch: any) => {
    dispatch(sendMessage(error));
    dispatch({type: UPLOAD_DOCUMENT_FAILURE, error});
  };

// document upload action
export const uploadDocument = (file: File) => (dispatch: any) => {
  DocumentApi.upload(file)
    .then((response: any) => dispatch(uploadSuccessful(response.data.document)))
    .catch((error: any) => {
      const {data} = error.response;

      // fallback error message
      let message = "Something happened";

      if (data && data.errors && data.errors.file && data.errors.file.length) {
        // check for regular laravel validation errors
        message = data.errors.file[0];
      } else if (data && data.message) {
        // check for generic error message
        message = data.message.toString();
      }

      dispatch(uploadFailed(message));
    });
};

/**
 * Document receiving
 */

// receive successful action
export const receiveDocumentsSuccess = (documents: Document[]) => {
  return {type: RECEIVE_DOCUMENT_SUCCESS, documents};
};

// receive failed action
export const receiveDocumentsFailed =
  (error: any) => (dispatch: any) => {
    dispatch(sendMessage("Something happened"));
    dispatch({type: RECEIVE_DOCUMENT_FAILURE, error});
  };

// get all documents action
export const getDocuments = () => (dispatch: any) => {
  DocumentApi.list()
    .then((response: any) => dispatch(receiveDocumentsSuccess(response.data.documents)))
    .catch((error: any) => {
      const message = error.response && error.response.data ?
        error.response.data : "Unexpected response from server"
      dispatch(receiveDocumentsFailed(message))
    });
};

export const searchDocuments = (text: string) => (dispatch: any) => {
  DocumentApi.list(text)
    .then((response: any) => dispatch(receiveDocumentsSuccess(response.data.documents)))
    .catch((error: any) => dispatch(receiveDocumentsFailed(error.response.data)));
};

/**
 * Document deleting
 */

// upload successful action
export const deleteSuccessful = (id: number) => {
  return (dispatch: any) => {
    dispatch(sendMessage("Document deleted"));
    dispatch({type: DELETE_DOCUMENT_SUCCESS, id});
  }
};

// upload failed action
export const deleteFailed =
  (error: any) => (dispatch: any) => {
    dispatch(sendMessage("Something happened"));
    dispatch({type: DELETE_DOCUMENT_FAILURE, error});
  };

// document upload action
export const deleteDocument = (id: number) => (dispatch: any) => {
  DocumentApi.delete(id)
    .then(() => {
      dispatch(deleteSuccessful(id))
    })
    .catch((error: any) => {
      dispatch(deleteFailed(error.response.data))
    });
};

/**
 * Notification actions
 */

// send notification
export const sendMessage = (message: string) =>
  ({type: SEND_MESSAGE, message});

// dismiss notification
export const dismissMessage = (message: string) =>
  ({type: DISMISS_MESSAGE, message});
