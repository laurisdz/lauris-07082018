import axios from 'axios'

class DocumentApi {
  static upload(file: File) {
    const data = new FormData();
    data.append('file', file);

    return axios.post('/api/document', data);
  }

  static list(search: string = "") {
    const params: any = {};
    if (search.length) {
      params.search = search;
    }
    return axios.get('/api/document', {params});
  }

  static delete(id: number) {
    return axios.delete(`/api/document/${id}`);
  }
}

export default DocumentApi;