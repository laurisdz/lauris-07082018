import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import App from './components/App';
import rootReducer from './reducers';
import {getDocuments} from "./actions";

// configure redux store
const store = createStore(rootReducer,
  applyMiddleware(thunk));

// load initial data
store.dispatch(getDocuments());

// render react app to root element
ReactDOM.render(
  <Provider store={store}><App/></Provider>,
  document.getElementById('root')
);