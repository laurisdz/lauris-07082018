import * as React from "react";
import DocumentList from './DocumentList';
import CreateDocument from './CreateDocument';
import Notification from './Notification';
import SearchBar from './SearchBar';
// import Grid from '@material-ui/core/Grid';

export default class App extends React.Component {
  style = {
    root: {
      maxWidth: "700px",
      margin: "2rem auto"
    },
    topBar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between"
    }
  };

  render() {
    return (
      <div style={this.style.root}>
        <div style={this.style.topBar}>
          <CreateDocument/>
          <SearchBar/>
        </div>
        <DocumentList/>
        <Notification/>
      </div>
    )
  };
}
