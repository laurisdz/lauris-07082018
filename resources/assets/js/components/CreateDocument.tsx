import * as React from "react";
import {connect} from "react-redux";
import {uploadDocument} from "../actions";

import {Button} from '@material-ui/core';

const mapDispatchToProps = (dispatch: any) => {
  return {
    uploadDocument: (file: File) => {
      dispatch(uploadDocument(file))
    }
  };
};

interface Props {
  uploadDocument: any;
}

interface State {
  value: string
}

class _CreateDocument extends React.Component<Props, State> {
  // local styles
  style = {
    input: {display: "none"}
  };

  // local state
  state = {
    value: ""
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const {files} = event.currentTarget;

    if (files && files.length) {
      this.props.uploadDocument(files[0]);
    }

    // reset local state so we can upload the same file again
    this.setState({value: ""});
  };

  render() {
    return (
      <div>
        <label>
          <Button variant="raised" color="primary" component="span">
            <input
              accept="image/*"
              style={this.style.input}
              onChange={this.handleChange}
              value={this.state.value}
              type="file"
            />
            Upload
          </Button>
        </label>
      </div>
    );
  }
}

const CreateDocument = connect(null, mapDispatchToProps)(_CreateDocument);
export default CreateDocument;
