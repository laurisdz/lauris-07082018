import * as React from "react";
import {Document} from "../types"

import {
  Image,
  Delete,
} from '@material-ui/icons';

import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Avatar,
} from '@material-ui/core';

import {deleteDocument} from "../actions";
import {connect} from "react-redux";

interface Props {
  document: Document;
  deleteDocument: any;
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    deleteDocument: (id: number) => {
      dispatch(deleteDocument(id))
    }
  };
};

class _DocumentItem extends React.Component<Props, {}> {

  delete = (doc: Document) => {
    console.log("delete document", doc);
    this.props.deleteDocument(doc.id);
  };

  render() {
    const doc = this.props.document;
    return (
      <ListItem>
        <Avatar>
          <Image/>
        </Avatar>
        <ListItemText primary={doc.title} secondary={`Type: ${doc.type} Created: ${doc.created_at}`}/>
        <ListItemSecondaryAction>
          <IconButton onClick={() => this.delete(doc)} aria-label="Delete">
            <Delete/>
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
    )
  }
}

const DocumentItem = connect(null, mapDispatchToProps)(_DocumentItem);
export default DocumentItem;
