import * as React from "react";
import {connect} from "react-redux";
import {Document} from "../types"

import {List, Paper, Theme, Typography} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';
import DocumentItem from './DocumentItem'

const mapStateToProps = (state: any) => {
  return {documents: state.documents};
};

interface DocumentListProps {
  documents: Document[];
  classes: any;
}

const styles: any = (theme: Theme) => ({
  root: {
    ...theme.mixins.gutters(),
    marginTop: theme.spacing.unit * 2,
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
});

class DocumentList extends React.Component<DocumentListProps, {}> {
  render() {
    if (!this.props.documents.length) {
      const { classes } = this.props;
      return (
        <div>
          <Paper elevation={1} className={classes.root}>
            <Typography variant="subheading" component="h3">
              Hey, no documents found!
            </Typography>
          </Paper>
        </div>
      );
    }

    return (
      <div>
        <List>
          {this.props.documents.map((doc: Document, idx: number) => (
            <DocumentItem document={doc} key={idx}/>
          ))}
        </List>
      </div>
    )
  }
}

const styledComponent = withStyles(styles)(DocumentList);
export default connect(mapStateToProps)(styledComponent);
