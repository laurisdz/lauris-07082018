import * as React from "react";
import {SnackbarOrigin} from "@material-ui/core/Snackbar";
import {IconButton, Snackbar,} from '@material-ui/core';
import {Close} from '@material-ui/icons';
import {State} from "../reducers";
import {connect} from "react-redux";
import {dismissMessage} from "../actions";

const mapStateToProps = (state: State) => {
  return {messages: state.messages};
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    dismissMessage: (message: string) => {
      dispatch(dismissMessage(message))
    }
  };
};

interface Props {
  messages: string[];
  dismissMessage: any;
}

class _Notification extends React.Component<Props, {}> {
  snackbarPosition: SnackbarOrigin = {
    vertical: 'bottom',
    horizontal: 'center',
  };

  close = (message: any) => {
    this.props.dismissMessage(message);
  };

  render() {
    return (
      <div>
        {this.props.messages.map((message: string, idx: number) => (
          <Snackbar
            key={idx}
            anchorOrigin={this.snackbarPosition}
            open={true}
            autoHideDuration={6000}
            onClose={() => this.close(message)}
            message={<span>{message}</span>}
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={() => this.close(message)}
              >
                <Close/>
              </IconButton>,
            ]}
          />
        ))}
      </div>
    )
  };
}

const Notification = connect(mapStateToProps, mapDispatchToProps)(_Notification);
export default Notification;
