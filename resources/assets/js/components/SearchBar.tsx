import * as React from "react";
import {Document} from "../types"
import {TextField} from '@material-ui/core';
import {connect} from "react-redux";
import {searchDocuments} from "../actions";
import {debounce} from "../helpers";

const mapDispatchToProps = (dispatch: any) => {
  return {
    searchDocuments: (text: string) => {
      dispatch(searchDocuments(text))
    }
  };
};

interface Props {
  searchDocuments: any;
}

interface State {
  search: string;
}

class _SearchBar extends React.Component<Props, {}> {
  state: State = {
    search: ""
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const search = event.currentTarget.value;
    this.setState({search});

    // debounce search action
    debounce((search) => {
      console.log("debounced", this.props);
      this.props.searchDocuments(search);
    }, 400)(search);
  };

  render() {
    return (
      <div>
        <TextField
          placeholder="Search"
          type="search"
          margin="normal"
          onChange={this.handleChange}
          value={this.state.search}
        />
      </div>
    )
  }
}

const SearchBar = connect(null, mapDispatchToProps)(_SearchBar);
export default SearchBar;
