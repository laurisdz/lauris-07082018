/**
 * Debounce function
 * @param {F} fn Function to debounce
 * @param {number} delay Delay in mis
 * @returns {F}
 */
export function debounce<F extends (...params: any[]) => void>(fn: F, delay: number) {
  let timeout: number | null = null;
  return function (this: any, ...args: any[]) {
    if (timeout) {
      clearTimeout(timeout);
    }
    timeout = window.setTimeout(() => fn.apply(this, args), delay);
  } as F;
}

function throttle(fn: Function, wait: number) {
  let isCalled = false;

  return function (...args: any[]) {
    if (!isCalled) {
      fn(...args);
      isCalled = true;
      setTimeout(function () {
        isCalled = false;
      }, wait)
    }
  };
}