import {
  UPLOAD_DOCUMENT_SUCCESS,
  UPLOAD_DOCUMENT_FAILURE,
  RECEIVE_DOCUMENT_SUCCESS,
  DELETE_DOCUMENT_SUCCESS,
  DELETE_DOCUMENT_FAILURE,
  DISMISS_MESSAGE,
  SEND_MESSAGE
} from "../constants";
import {Document} from "../types";

export interface State {
  readonly documents: Document[];
  readonly messages: string[];
}

const initialState: State = {
  documents: [],
  messages: []
};

const rootReducer = (state = initialState, action: any) => {
  console.log("reducer action", action, state);

  switch (action.type) {
    // success actions
    case UPLOAD_DOCUMENT_SUCCESS:
      return {...state, documents: [action.document, ...state.documents]};
    case RECEIVE_DOCUMENT_SUCCESS:
      return {...state, documents: [...action.documents]};
    case DELETE_DOCUMENT_SUCCESS:
      return {
        ...state, documents: [
          ...state.documents
            .filter((doc: Document) => doc.id !== action.id)
        ]
      };

    // error actions
    case UPLOAD_DOCUMENT_FAILURE:
    case DELETE_DOCUMENT_FAILURE:
      return state;

    // notification actions
    case SEND_MESSAGE:
      return {...state, messages: [...state.messages, action.message]};
    case DISMISS_MESSAGE:
      return {
        ...state, messages: [
          ...state.messages
            .filter((m: string) => m !== action.message)
        ]
      };

    default:
      return state;
  }
};

export default rootReducer;