export interface StoreState {
  languageName: string;
  enthusiasmLevel: number;
}

export interface Document {
  id: number;
  title: string;
  type: string;
  created_at: string;
}