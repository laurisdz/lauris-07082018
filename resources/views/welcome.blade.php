<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Document Management Platform</title>
</head>
<body>
    <div id="root"></div>
    <script nonce="{{ csp_nonce() }}" src="{{mix('js/app.js')}}"></script>
</body>
</html>