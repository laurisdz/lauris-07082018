const mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

mix.disableSuccessNotifications();
mix.disableNotifications();

mix.webpackConfig({
  resolve: {
    extensions: ['*', '.js', '.jsx', '.ts', '.tsx'],
    alias: {
      "@": path.resolve(__dirname, './resources/assets/js')
    }
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  }
})

mix.react('resources/assets/js/app.tsx', 'public/js');

// mix.less('resources/assets/less/app.less', 'public/css', {
//   javascriptEnabled: true
// });
